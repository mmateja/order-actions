<?php

namespace App;

abstract class AbstractOrderAction
{
    //stałe określające nazwy akcji wysyłanych do zewnętrznych serwisów/aplikacji
    const ACTION_RESEND = 'resend';
    const ACTION_EDIT = 'edit';
    const ACTION_PAYMENT_SMS = 'resend_sms';
    const ACTION_PAYMENT_EMAIL = 'send_payment_url';
    const ACTION_COURIER_DELIVERY_ATTEMPT = 'dpd_next_attempt';
    const ACTION_COURIER_CHANGE_ADDRESS = 'dpd_change_address';
    const ACTION_COURIER_CANCEL = 'dpd_cancel';
    const ACTION_COURIER_CHANGE_PRICE = 'dpd_change_price';
    const ACTION_CANCEL = 'cancel_order';

    //akcje możliwe do wykonania jeśli zamówienie zostało juz wysłane
    const COURIER_ACTIONS = [
        self::ACTION_COURIER_CANCEL,
        self::ACTION_COURIER_DELIVERY_ATTEMPT,
        self::ACTION_COURIER_CHANGE_ADDRESS,
    ];

    //tekst na buttonie
    protected string $label;
    //klasa bootstrapa
    protected string $btnClass;
    //button posiadał akcję albo opcje. Jeśli miał akcję to był kliknięcie powodowało jej obsługę.
    protected ?string $action = null;
    //opcje to dropdown z akcjami. Przykład: Buttons/CourierMail.php
    protected array $options = [];
    //jeśli button miał zadeklarowane pola formularza, pojawiał się modal, żeby pobrać od użytkownika dodatkowe informacje
    protected array $formFields = [];

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getBtnClass(): string
    {
        return $this->btnClass;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @return array<int, OrderActionInterface>
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @return array<string, mixed>
     */
    public function getFormFields(): array
    {
        return $this->formFields;
    }
}
