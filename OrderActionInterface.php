<?php

namespace App;

interface OrderActionInterface
{
    public function getLabel(): string;
    public function getBtnClass(): string;
    public function getAction(): ?string;
    public function getOptions(): array;
    public function getFormFields(): array;
}
