const actionObj = {
    action: '',
    label: '',
    agent: '',
    ref: '',
    //nazwy pól narzucone przez webservice
    data: {
        out_akcja: '',
        Agent_Name_Surname: '',
    },
}
$('#history-wrapper').on('click', '.action', (event) => {
    event.preventDefault()
    const $this = $(event.target);
    const action = $this.data('action')
    const orderData = $this.closest('.card').data('order')
    const agent = $('#order_form_agent option:selected').text()
    actionObj.action = action
    actionObj.label = $this.text();
    actionObj.agent = agent
    actionObj.ref = orderData.reference
    actionObj.data.out_akcja = action
    actionObj.data.Agent_Name_Surname = agent
    actionObj.data = {
        ...actionObj.data,
        ...orderData,
    }

    const form = $this.data('form')
    if (!$.isEmptyObject(form)) {
        const $modal = $('#modal')
        const $modalBody = $modal.find('.modal-body')
        $modal.find('.save').addClass('send-action')
        $modal.find('.modal-title').html(form.label)
        let html = '<form>'
        form.fields.forEach((item) => {
            html += '<input type="'+item.type+'" name="'+item.name+'" class="form-control" placeholder="'+item.label+'" required />'
        })
        html += '</form>'
        $modalBody.html(html)
        $modal.modal('show')

        $modal.on('click', '.send-action', () => {
            const form = $modalBody.find('form').get(0)
            if (!form.checkValidity()) {
                alert('Uzupełnij wszystkie pola formularza')
                return
            }
            const data = new FormData($modalBody.find('form').get(0))
            const json = Object.fromEntries(data.entries())
            actionObj.data = {
                ...actionObj.data,
                ...json,
            }

            sendAction(actionObj)
            $modal.modal('hide')
        })
    } else {
        sendAction(actionObj)
    }
})

const sendAction = (action) => {
    const actionLabel = action.label
    delete action.label
    $.post('/api/order/action', {'action': action})
        .done(() => {
            toastr.success('Akcja "'+actionLabel+'" wykonana poprawnie.')
        })
        .fail(() => {
            toastr.success('Nie udało sie wykonać akcji "'+actionLabel+'".')
        })
}
