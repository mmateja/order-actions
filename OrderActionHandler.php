<?php

namespace App;

use App\Utils\Bitrix\BitrixApiService;
use App\Utils\Bridge\BridgeApiService;
use App\Utils\Shop\API\ApiResult;
use App\Utils\Shop\API\ShopApiService;

class OrderActionHandler
{
    public function __construct(
        private readonly ShopApiService $shopApiService,
        private readonly BitrixApiService $bitrixApiService,
        private readonly BridgeApiService $bridgeApiService,
    ) {
    }

    public function sendAction(array $action): ApiResult
    {
        //weryfikacja czy akcja dotyczy kuriera (wysłanej paczki)
        if (in_array($action['action'], AbstractOrderAction::COURIER_ACTIONS)) {
            $shippedTodayResult = $this->shopApiService->isShippedToday($action['data']);
            if (!empty($shippedTodayResult->getErrorMessage())) {
                return $shippedTodayResult;
            }

            $shippedToday = $shippedTodayResult->getData()['shipped_today'];
            //jeśli wysyłka nastąpiła dziś, powiadom magazyn o konieczności rozpakowania paczki
            if ($shippedToday) {
                $orderDataResult = $this->shopApiService->getNotificationOrderData($action['data']['id_order']);
                if (!empty($orderDataResult->getErrorMessage())) {
                    return $shippedTodayResult;
                }

                $this->bitrixApiService->sendMessage($this->createMessage($action, $orderDataResult->getData()), BitrixApiService::ORDER_SERVICE);
            }

            //wyślij maila do kuriera
            if (!$shippedToday || $action['action'] === AbstractOrderAction::ACTION_COURIER_CHANGE_ADDRESS) {
                return $this->bridgeApiService->createPostRequest('---url---', $action['data'], true);
            }
        } elseif (AbstractOrderAction::ACTION_COURIER_CHANGE_PRICE === $action['action']) {
            //zmień cenę
            return $this->bridgeApiService->createPostRequest('---url---', $action['data'], true);
        } else {
            //sklep miał dopisany webservice do wykonywania różnych akcji na zamówieniu
            return $this->shopApiService->sendAction($action);
        }

        $result = new ApiResult();
        $result->setErrorMessage('Nie udało się obsłużyć akcji');

        return $result;
    }

    private function createMessage(array $action, array $orderData): string
    {
        $trackingNumber = $action['data']['tracking_number'];
        $agent = $action['agent'];
        $action = $action['action'];
        $message = match ($action) {
            AbstractOrderAction::ACTION_COURIER_CANCEL => sprintf('WYMAGANA AKCJA%sZamówienie %s zostało anulowane.', \PHP_EOL, $orderData['orderNumber']),
            AbstractOrderAction::ACTION_COURIER_CHANGE_ADDRESS => sprintf('INFO%sW zamówieniu %s został zmieniony adres dostawy.', \PHP_EOL, $orderData['orderNumber']),
            AbstractOrderAction::ACTION_COURIER_DELIVERY_ATTEMPT => sprintf('INFO%sWysłano prośbę o kolejną próbę doręczenia zamówienia %s.', \PHP_EOL, $orderData['orderNumber']),
            default => sprintf('INFO%3$sZamówienie: %s, %3$sAkcja:%s', $orderData['orderNumber'], $action, \PHP_EOL),
        };

        return sprintf(
            '%s%6$sKlient: %s,%6$sSklep: %s,%6$sNumer przesyłki: %s,%6$sAgent: %s',
            $message,
            $orderData['client'],
            $orderData['shopUrl'],
            $trackingNumber,
            $agent,
            \PHP_EOL,
        );
    }
}
