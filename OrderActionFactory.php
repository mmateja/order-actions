<?php

declare(strict_types=1);

namespace App;

use App\Buttons\Cancel;
use App\Buttons\CourierMail;
use App\Buttons\Edit;
use App\Buttons\PaymentLink;
use App\Buttons\Resend;

class OrderActionFactory
{
    public const STATE_PREPARATION = 3;
    public const STATE_SENT = 4;
    public const STATE_CANCELED = 6;
    public const STATE_RETURNED = 7;
    public const STATE_ONP = 15;
    public const STATE_RCZTB = 18;
    public const STATE_POTENTIAL_RETURN = 19;

    /**
     * @return array<int, OrderActionInterface>
     */
    public static function availableActions(int $orderState): array
    {
        //w zależności od statusu, na którym aktualnie znajduje się zamówienie agent mógł wykonać na nim określone akcje. Kilka przykładów:
        //jeśli anulowane albo zwrot -> wyślij ponownie
        //jeśli w przygotowaniu albo oczekuje na płatność -> edytuj, wyślij link do płatności, anuluj
        //jeśli wysłane -> wyślij maila do kuriera
        return match (true) {
            \in_array($orderState, [self::STATE_CANCELED, self::STATE_RETURNED], true) => [new Resend()],
            \in_array($orderState, [self::STATE_PREPARATION, self::STATE_ONP], true) => [new Edit(), new PaymentLink(), new Cancel()],
            self::STATE_RCZTB === $orderState, \in_array($orderState, [self::STATE_SENT, self::STATE_POTENTIAL_RETURN], true) => [new CourierMail()],
            default => [],
        };
    }
}
