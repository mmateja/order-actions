<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class CourierCancel extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'Anulowanie zamówienia';
        $this->btnClass = 'danger';
        $this->action = self::ACTION_COURIER_CANCEL;
    }
}
