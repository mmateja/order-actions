<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class Cancel extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'Anuluj zamówienie';
        $this->btnClass = 'danger';
        $this->action = self::ACTION_CANCEL;
        $this->formFields = [
            'label' => 'Powód anulowania',
            'fields' => [
                ['name' => 'cancellation_reason', 'label' => 'Podaj powód', 'type' => 'text'],
            ]
        ];
    }
}
