<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class CourierMail extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'Akcje na przesyłce';
        $this->btnClass = 'secondary';
        $this->options = [new CourierCancel(), new CourierDeliveryAttempt(), new CourierChangeAddress()];
    }
}
