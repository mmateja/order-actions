<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class SMSPaymentLink extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'SMS';
        $this->btnClass = 'dark';
        $this->action = self::ACTION_PAYMENT_SMS;
    }
}
