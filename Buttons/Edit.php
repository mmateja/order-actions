<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class Edit extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'Edytuj';
        $this->btnClass = 'secondary';
        $this->action = self::ACTION_EDIT;
    }
}
