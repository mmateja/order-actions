<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class Resend extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'Wyślij ponownie';
        $this->btnClass = 'secondary';
        $this->action = self::ACTION_RESEND;
    }
}
