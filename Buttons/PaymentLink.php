<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class PaymentLink extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'Wyślij link do płatności';
        $this->btnClass = 'secondary';
        $this->options = [new SMSPaymentLink(), new EmailPaymentLink()];
    }
}
