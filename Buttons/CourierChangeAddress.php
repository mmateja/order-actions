<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class CourierChangeAddress extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'Zmiana adresu dostawy';
        $this->btnClass = 'dark';
        $this->action = self::ACTION_COURIER_CHANGE_ADDRESS;
        $this->formFields = [
            'label' => 'Nowy adres dostawy',
            'fields' => [
                ['name' => 'out_name', 'label' => 'Imię', 'type' => 'text'],
                ['name' => 'out_surname', 'label' => 'Nazwisko', 'type' => 'text'],
                ['name' => 'out_address', 'label' => 'Adres', 'type' => 'text'],
                ['name' => 'out_postcode', 'label' => 'Kod pocztowy', 'type' => 'text'],
                ['name' => 'out_city', 'label' => 'Miasto', 'type' => 'text'],
            ]
        ];
    }
}
