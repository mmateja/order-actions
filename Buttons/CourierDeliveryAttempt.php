<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class CourierDeliveryAttempt extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'Kolejna próba dostarczenia';
        $this->btnClass = 'dark';
        $this->action = self::ACTION_COURIER_DELIVERY_ATTEMPT;
    }
}
