<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class CourierChangePrice extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'Zmiana ceny';
        $this->btnClass = 'dark';
        $this->action = self::ACTION_COURIER_CHANGE_PRICE;
        $this->formFields = [
            'label' => 'Nowa cena',
            'fields' => [
                ['name' => 'out_new_price', 'label' => 'Cena', 'type' => 'number'],
            ],
        ];
    }
}
