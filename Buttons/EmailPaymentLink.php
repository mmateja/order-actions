<?php

namespace App\Buttons;

use App\AbstractOrderAction;
use App\OrderActionInterface;

class EmailPaymentLink extends AbstractOrderAction implements OrderActionInterface
{
    public function __construct()
    {
        $this->label = 'E-mail';
        $this->btnClass = 'dark';
        $this->action = self::ACTION_PAYMENT_EMAIL;
    }
}
